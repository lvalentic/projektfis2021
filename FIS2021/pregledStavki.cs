﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace FIS2021
{
    public partial class pregledStavki : Form
    {
        public pregledStavki()
        {
            InitializeComponent();

        }

        
        private void btnJson_Click(object sender, EventArgs e)
        {
            var converter = new Converter();
            rchtextbox.Text = converter.toJson(rchtextbox.Text);
            
        }

        private void btnXml_Click(object sender, EventArgs e)
        {
            /*
            var converter = new Converter();
            rchtextbox.Text = converter.toXml(rchtextbox.Text).ToString() ;
            */

            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.InitialDirectory = "c:\\";
                openFileDialog.Filter = "XML Files (*.xml)|*.xml";
                openFileDialog.FilterIndex = 0;
                openFileDialog.RestoreDirectory = true;
                var response = "";
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    //Get the path of specified file
                    string filePath = openFileDialog.FileName;

                    //Read the contents of the file into a stream
                    var fileStream = openFileDialog.OpenFile();

                    using (StreamReader reader = new StreamReader(fileStream))
                    {
                       string fileContent = reader.ReadToEnd();
                        response = fileContent;
                    }
                    rchtextbox.Text = response;
                }
            }

            
        }

        private void pregledStavki_Load(object sender, EventArgs e)
        {
            rchtextbox.Text = @"{
  '@Id': 1,
  'Email': 'james@example.com',
  'Active': true,
  'CreatedDate': '2013-01-20T00:00:00Z',
  'Roles': [
    'User',
    'Admin'
  ],
  'Team': {
    '@Id': 2,
    'Name': 'Software Developers',
    'Description': 'Creators of fine software products and services.'
  }
}";
        }

        private void btnFind_Click(object sender, EventArgs e)
        {
            var trazi = new Trazilica();
            rchtextbox.Text = trazi.pronadiStavku(txtboxFind.Text);

            
        }

        private void btnSve_Click(object sender, EventArgs e)
        {
            var trazi = new Trazilica();
            List<string> rez = trazi.Sve();

            foreach( var x in rez)
            {
                rchtextbox.Text = x + Environment.NewLine + rchtextbox.Text + Environment.NewLine;
            }

        }

        private void btnToXml_Click(object sender, EventArgs e)
        {
            /*
            string test = "<body><head>" +rchtextbox.Text + "</head></body>";
            XmlDocument xmltest = new XmlDocument();
            xmltest.LoadXml(test);
            xmltest.PreserveWhitespace = true;
            xmltest.Save(@"C:\Users\Leo\Desktop\xmldata.xml");​
            */

            string x ="<root>"+rchtextbox.Text+"</root>";
            XmlDocument xd = new XmlDocument();
            xd.LoadXml(x);
            xd.Save(@"C:\Users\Leo\Desktop\xmldata" + DateTime.Now.ToFileTime() +".xml");

        }
    }
}
