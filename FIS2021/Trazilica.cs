﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FIS2021
{
    class Trazilica
    {

        


        public string pronadiStavku(string trazim)
        {
            string query = "SELECT nazivStavke,IDstavke,cijenaStavke FROM dbo.stavkeTable WHERE nazivStavke = '" + trazim + "'"; ;
            var conn = new dbConnect();
            string konekcijskiString = conn.connString;
            string response = "";

            var table = new DataTable();

            using ( var da = new SqlDataAdapter(query,konekcijskiString))
            {
                da.Fill(table);
                foreach (DataRow row in table.Rows)
                {
                    string naziv = row["nazivStavke"].ToString() + Environment.NewLine;
                    string ID = row["IDstavke"].ToString() + Environment.NewLine;
                    string cijena = row["cijenaStavke"].ToString();

                    response = "Naziv stavke koju ste trazili : " + naziv + "ID stavke je : "  + ID +"Cijena stavke je : " + cijena +"Kn";
                    
                }

                return response + Environment.NewLine + "Vrijeme izvrsenja : " + DateTime.Now.ToLocalTime();
            }
           
    

        }

        public List<string> Sve()
        {
            string query = "SELECT nazivStavke,IDstavke,cijenaStavke FROM dbo.stavkeTable"; 
            var conn = new dbConnect();
            string konekcijskiString = conn.connString;
            string response = "";
            List<string> listaStavki = new List<string>();
            var table = new DataTable();

            using (var da = new SqlDataAdapter(query, konekcijskiString))
            {
                da.Fill(table);
                foreach (DataRow row in table.Rows)
                {
                   
                        string naziv = row["nazivStavke"].ToString() + Environment.NewLine;
                        string ID = row["IDstavke"].ToString() + Environment.NewLine;
                        string cijena = row["cijenaStavke"].ToString();

                        response = "ID stavke : " + ID + "Naziv stavke : " + naziv +"Cijena stavke : " + cijena + Environment.NewLine;
                    listaStavki.Add(response);

                }

                return listaStavki;
            }



        }

    }

}
