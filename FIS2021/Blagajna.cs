﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FIS2021
{
    class Blagajna : Stavke
    {
        
       
        public struct izracunRacuna
        {
           public double uplaceniNovac;
           public double zaIzrvratit;
        }

        /*
        public string izracunaj(izracunRacuna racun)
        {

            if (stavke.cijenaStavke > racun.uplaceniNovac)
            {
                return "Nije uplaceno dovoljno novca!";
            }
            else

            racun.zaIzrvratit = stavke.cijenaStavke - racun.uplaceniNovac;

            return "Izvratiti je potrebno" + racun.zaIzrvratit +"Kn";
        }
        */

       
      
        public string izracunaj(double a, double b)
        {
            var stavke = new Stavke();
            izracunRacuna racun;

            stavke.cijenaStavke = a;
            racun.uplaceniNovac = b;

            if (stavke.cijenaStavke > racun.uplaceniNovac)
            {
                return "Nije uplaceno dovoljno novca!";
            }
            else

                racun.zaIzrvratit = stavke.cijenaStavke - racun.uplaceniNovac;

            return "Izvratiti je potrebno " + racun.zaIzrvratit + "Kn";
        }
       
    }
}
