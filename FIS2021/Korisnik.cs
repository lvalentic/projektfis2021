﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FIS2021
{
    class Korisnik
    {
        private string imeKorisnika;

        private string prezimeKorisnika;

        private int godinaRodenja;

        private string lozinkaKorisnika;

        public Korisnik()
        {

        }
        public Korisnik(string imeKorisnika,string prezimeKorisnika,int godinaRodenja,string lozinkaKorisnika)
        {
            var korisnik = new Korisnik();
            korisnik.imeKorisnika = this.imeKorisnika;
            korisnik.prezimeKorisnika = this.prezimeKorisnika;
            korisnik.godinaRodenja = this.godinaRodenja;
            korisnik.lozinkaKorisnika = this.lozinkaKorisnika;

            izradiKorisnika(korisnik);
        }

      
        //JOS NIJE SLOZENO DA SE MOZE IZRADITI NOVI KORISNIK, OVAJ KOD JE OPCENITO BIO KORISTEN ZA OFFLINE PROVJERU PRI POCETKU IZRADE
      

        public Korisnik izradiKorisnika(Korisnik noviKorisnik)
        {
            if (this.imeKorisnika != "" && this.prezimeKorisnika != "" && this.godinaRodenja != 0 && this.lozinkaKorisnika != "")
            {
                this.imeKorisnika = noviKorisnik.imeKorisnika;
                this.prezimeKorisnika = noviKorisnik.prezimeKorisnika;
                this.godinaRodenja = noviKorisnik.godinaRodenja;
                this.lozinkaKorisnika = noviKorisnik.lozinkaKorisnika;
                //listaKorisnika.Add(noviKorisnik);
                return noviKorisnik;
            }
            else
            {
                return null;
            }
        }
        
        /*
        public List<Korisnik> listaKorisnika;
        public bool ispravanKorisnik;

        public void provjeriKorisnike()
        {
            foreach (var x in listaKorisnika)
            {
                //isto ko stavke
            }
        }

        public bool provjeriKorisnika(Korisnik korisnik)
        {

            foreach (var x in listaKorisnika)
            {

                if (korisnik.imeKorisnika == x.imeKorisnika && korisnik.lozinkaKorisnika == x.lozinkaKorisnika)
                {
                    ispravanKorisnik = true;

                }
                else
                {
                    ispravanKorisnik = false;

                }


            }

            return ispravanKorisnik;

        }
        */
    }
}

