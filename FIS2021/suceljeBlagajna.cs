﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FIS2021
{
    public partial class suceljeBlagajna : Form
    {
        public suceljeBlagajna() 
        {
            InitializeComponent();
            
        }

        public string loginKorisnik;
        private void btnIzracunaj_Click(object sender, EventArgs e)
        {
            char[] charsToTrim = { 'K', 'n', };

            if ( lblCijena.Text == "Cijena")
            {
                MessageBox.Show("Niste dodali ni jedan proizvod u košaricu!");
                
            }

            else
            {
                var blagajna = new Blagajna();
                var stavka = new Stavke();

                double iznos;
                Double.TryParse(lblCijena.Text.Trim(charsToTrim), out iznos);

                double uplaceno;
                Double.TryParse(txtUplaceno.Text, out uplaceno);



                lblObracun.Text = blagajna.izracunaj(iznos, uplaceno);

                var racunko = new Racun();

                racunko.postaviRacun(rchTextbox.Text, lblTrenutniKorisnik.Text, Convert.ToInt32(lblCijena.Text.Trim(charsToTrim)));
            }


            
         
   
            
        }

     

        private void btnTraziStavku_Click(object sender, EventArgs e)
        {

            char[] charsToTrim = { 'K', 'n', };
            var stavka = new Stavke();
            string nazivStavke = txtStavka.Text.Trim();
            stavka.pronadiStavku(nazivStavke);

            txtboxIDstavke.Text = stavka.ID.ToString() + ".";
            txtBoxNazivStavke.Text = stavka.nazivStavki;
            txtboxCijenaStavke.Text = stavka.cijenaStavke.ToString() + " Kn";


          
            
        }

        private void Naplata_Click(object sender, EventArgs e)
        {
            char[] charsToTrim = { 'K', 'n','.'};
            string txtbox = txtboxCijenaStavke.Text.Trim(charsToTrim);
            string lbl = lblCijena.Text.Trim(charsToTrim);
            int a, b,c;

            int.TryParse(lbl, out a);
            int.TryParse(txtbox, out b);

            c = a + b;
            lblCijena.Text = c.ToString() + " Kn";

            string prvitekst = rchTextbox.Text;
            rchTextbox.Text = "Stavka : " + txtBoxNazivStavke.Text + " > > > > Cijena stavke : " + txtboxCijenaStavke.Text + Environment.NewLine;
            rchTextbox.Text = prvitekst + Environment.NewLine + rchTextbox.Text;
        }

        private void txtIznos_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            lblCijena.Text = "Cijena";
            txtStavka.Text = "";
            txtBoxNazivStavke.Text = "";
            txtboxIDstavke.Text = "";
            txtboxCijenaStavke.Text = "";
            txtUplaceno.Text = "";
            rchTextbox.Text = "";
            lblObracun.Text = "";
        }

        private void suceljeBlagajna_Load(object sender, EventArgs e)
        {
            lblTrenutniKorisnik.Text = loginKorisnik;
        }
    }
}
