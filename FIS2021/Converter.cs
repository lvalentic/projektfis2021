﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using Newtonsoft.Json;

namespace FIS2021
{
    class Converter
    {
        public string toJson(string xml)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);
                string json = JsonConvert.SerializeXmlNode(doc);
                return json;
            }

            catch(Exception e)
            {
                throw e;
            }
            

            
        }

        public XNode toXml(string json)
        {
            XNode node = JsonConvert.DeserializeXNode(json,"Root");
            return node;
        }

      
    }
}
