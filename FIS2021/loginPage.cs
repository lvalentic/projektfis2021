﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace FIS2021
{
    public partial class loginPage : Form
    {
        public loginPage()
        {
            InitializeComponent();
        }


        private void Form1_Load(object sender, EventArgs e)
        {
            lblDateTime.Text = DateTime.Now.ToLongDateString();
            lblIp.Text = GetLocalIPAddress();
        }


        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("No network adapters with an IPv4 address in the system!");
        }

        
        
        private void btnLogin_Click(object sender, EventArgs e)
        {

            var korisnikLogin = new dbConnect();
            string username = txtUsername.Text.Trim();
            string password = txtPassword.Text.Trim();
            korisnikLogin.connLogin(username, password);
            this.Hide();


        

        }
    }
}
