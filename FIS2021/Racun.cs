﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FIS2021
{
    class Racun
    {

        private int ID;
        private string nazivStavke;
        private string racunIzdao;
        private int ukupanIznos;
        
        

        public Racun()
        {
            ID = 0;
            nazivStavke = "";
            racunIzdao = "";
            ukupanIznos = 0;
        }

        public Racun(int ID, string nazivStavke, string racunIzdao,int ukupanIznos)
        {

            var racun = new Racun();
            racun.ID = this.ID;
            racun.nazivStavke = this.nazivStavke;
            racun.racunIzdao = this.racunIzdao;
            racun.ukupanIznos = this.ukupanIznos;

            postaviRacun(racun.nazivStavke, racun.racunIzdao, racun.ukupanIznos);

        }

        



        public void postaviRacun(string naziv, string izdaoRacun, int iznos)
        {
             
            var conn = new dbConnect();
            string connString = conn.connString;
            SqlConnection sqlcon = new SqlConnection(connString);

            string query = "INSERT INTO dbo.Racun(ID,nazivStavke,racunIzdao,ukupanIznos,datumIzdavanja) VALUES(@ID,@nazivStavke,@racunIzdao,@ukupanIznos,@datumIzdavanja)";
            SqlCommand cmd = new SqlCommand(query, sqlcon);
            cmd.Parameters.Add("@ID", SqlDbType.Int);
            cmd.Parameters.Add("@nazivStavke", SqlDbType.VarChar, 255);
            cmd.Parameters.Add("@racunIzdao", SqlDbType.VarChar, 255);
            cmd.Parameters.Add("@ukupanIznos", SqlDbType.Int);
            cmd.Parameters.Add("@datumIzdavanja", SqlDbType.DateTime);

            sqlcon.Open();
            SqlCommand checkID = new SqlCommand("SELECT COUNT(*) FROM Racun", sqlcon);
            
            int IDpostoji = (int)checkID.ExecuteScalar();

            if (IDpostoji > 0)
            {
               
                cmd.Parameters["@ID"].Value = IDpostoji +1;
                cmd.Parameters["@nazivStavke"].Value = naziv;
                cmd.Parameters["@racunIzdao"].Value = izdaoRacun;
                cmd.Parameters["@ukupanIznos"].Value = iznos;
                cmd.Parameters["@datumIzdavanja"].Value = DateTime.Now.ToLocalTime();

                cmd.ExecuteNonQuery();
            }
            else
            {
                
                cmd.Parameters["@ID"].Value = 1;
                cmd.Parameters["@nazivStavke"].Value = naziv;
                cmd.Parameters["@racunIzdao"].Value = izdaoRacun;
                cmd.Parameters["@ukupanIznos"].Value = iznos;
                cmd.Parameters["@datumIzdavanja"].Value = DateTime.Now.ToLocalTime();

                cmd.ExecuteNonQuery();
            }

           
        }



        public List<string> dohvatiRacun()
        {

            var conn = new dbConnect();
            string connString = conn.connString;
            SqlConnection sqlcon = new SqlConnection(connString);

            sqlcon.Open();

            string query = "SELECT nazivStavke,ID,ukupanIznos,racunIzdao,datumIzdavanja FROM Racun";
            /*
            using (SqlCommand cmd = new SqlCommand(query, sqlcon))
            using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
            {
                DataTable dataTable = new DataTable();
                dataTable.Load(reader);
                return dataTable;
            }
            */

            var dataTable = new DataTable();
            List<string> listaRacuna = new List<string>();
            string response = "";

            using (var adp = new SqlDataAdapter(query,sqlcon))
            {
                adp.Fill(dataTable);

                foreach (DataRow row in dataTable.Rows)
                {

                    string naziv = row["nazivStavke"].ToString();
                    string ID = row["ID"].ToString() + Environment.NewLine;
                    string cijena = row["ukupanIznos"].ToString() +"Kn" + Environment.NewLine;
                    string racunKorisnik = row["racunIzdao"].ToString() + Environment.NewLine;
                    string datum = row["datumIzdavanja"].ToString() + Environment.NewLine;

                    response = "ID računa : " + ID + "Popis stavaka računa : " + naziv + "Ukupna cijena : " + cijena + "Racun izdao : " + racunKorisnik + "Datum izdavanja : " + datum + Environment.NewLine;
                    listaRacuna.Add(response);

                }

                return listaRacuna;
                
            }

        }

    }
}
