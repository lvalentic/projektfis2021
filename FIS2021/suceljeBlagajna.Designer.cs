﻿using System;

namespace FIS2021
{
    partial class suceljeBlagajna
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(suceljeBlagajna));
            this.txtStavka = new System.Windows.Forms.TextBox();
            this.txtUplaceno = new System.Windows.Forms.TextBox();
            this.lblObracun = new System.Windows.Forms.Label();
            this.btnIzracunaj = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblTrenutniKorisnik = new System.Windows.Forms.Label();
            this.rchTextbox = new System.Windows.Forms.RichTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.Naplata = new System.Windows.Forms.Button();
            this.btnTraziStavku = new System.Windows.Forms.Button();
            this.lblCijena = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtboxCijenaStavke = new System.Windows.Forms.TextBox();
            this.txtBoxNazivStavke = new System.Windows.Forms.TextBox();
            this.txtboxIDstavke = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtStavka
            // 
            this.txtStavka.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.txtStavka.Location = new System.Drawing.Point(3, 3);
            this.txtStavka.Multiline = true;
            this.txtStavka.Name = "txtStavka";
            this.txtStavka.Size = new System.Drawing.Size(537, 41);
            this.txtStavka.TabIndex = 0;
            this.txtStavka.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtStavka.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // txtUplaceno
            // 
            this.txtUplaceno.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.txtUplaceno.Location = new System.Drawing.Point(650, 149);
            this.txtUplaceno.Name = "txtUplaceno";
            this.txtUplaceno.Size = new System.Drawing.Size(471, 29);
            this.txtUplaceno.TabIndex = 0;
            this.txtUplaceno.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblObracun
            // 
            this.lblObracun.AutoSize = true;
            this.lblObracun.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblObracun.Location = new System.Drawing.Point(645, 241);
            this.lblObracun.Name = "lblObracun";
            this.lblObracun.Size = new System.Drawing.Size(129, 25);
            this.lblObracun.TabIndex = 1;
            this.lblObracun.Text = "Za izvratiti : ";
            // 
            // btnIzracunaj
            // 
            this.btnIzracunaj.Location = new System.Drawing.Point(813, 184);
            this.btnIzracunaj.Name = "btnIzracunaj";
            this.btnIzracunaj.Size = new System.Drawing.Size(152, 29);
            this.btnIzracunaj.TabIndex = 2;
            this.btnIzracunaj.Text = "Naplati";
            this.btnIzracunaj.UseVisualStyleBackColor = true;
            this.btnIzracunaj.Click += new System.EventHandler(this.btnIzracunaj_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.panel1.Controls.Add(this.lblTrenutniKorisnik);
            this.panel1.Controls.Add(this.rchTextbox);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.txtboxCijenaStavke);
            this.panel1.Controls.Add(this.txtBoxNazivStavke);
            this.panel1.Controls.Add(this.txtboxIDstavke);
            this.panel1.Controls.Add(this.btnIzracunaj);
            this.panel1.Controls.Add(this.txtUplaceno);
            this.panel1.Controls.Add(this.lblObracun);
            this.panel1.Cursor = System.Windows.Forms.Cursors.Default;
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1124, 535);
            this.panel1.TabIndex = 3;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // lblTrenutniKorisnik
            // 
            this.lblTrenutniKorisnik.AutoSize = true;
            this.lblTrenutniKorisnik.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblTrenutniKorisnik.Location = new System.Drawing.Point(968, 516);
            this.lblTrenutniKorisnik.Name = "lblTrenutniKorisnik";
            this.lblTrenutniKorisnik.Size = new System.Drawing.Size(123, 16);
            this.lblTrenutniKorisnik.TabIndex = 10;
            this.lblTrenutniKorisnik.Text = "Trenutni Korisnik";
            // 
            // rchTextbox
            // 
            this.rchTextbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.rchTextbox.Location = new System.Drawing.Point(10, 362);
            this.rchTextbox.Name = "rchTextbox";
            this.rchTextbox.Size = new System.Drawing.Size(537, 170);
            this.rchTextbox.TabIndex = 9;
            this.rchTextbox.Text = "";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(646, 122);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(117, 24);
            this.label2.TabIndex = 8;
            this.label2.Text = "Uplaćeno : ";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Gainsboro;
            this.panel2.Controls.Add(this.button1);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.Naplata);
            this.panel2.Controls.Add(this.btnTraziStavku);
            this.panel2.Controls.Add(this.lblCijena);
            this.panel2.Controls.Add(this.txtStavka);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1124, 100);
            this.panel2.TabIndex = 7;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(215, 50);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(116, 32);
            this.button1.TabIndex = 6;
            this.button1.Text = "Poništi";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Consolas", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(643, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(322, 41);
            this.label1.TabIndex = 1;
            this.label1.Text = "Ukupna cijena : ";
            // 
            // Naplata
            // 
            this.Naplata.Location = new System.Drawing.Point(337, 50);
            this.Naplata.Name = "Naplata";
            this.Naplata.Size = new System.Drawing.Size(203, 32);
            this.Naplata.TabIndex = 5;
            this.Naplata.Text = "Dodaj";
            this.Naplata.UseVisualStyleBackColor = true;
            this.Naplata.Click += new System.EventHandler(this.Naplata_Click);
            // 
            // btnTraziStavku
            // 
            this.btnTraziStavku.Location = new System.Drawing.Point(6, 50);
            this.btnTraziStavku.Name = "btnTraziStavku";
            this.btnTraziStavku.Size = new System.Drawing.Size(203, 32);
            this.btnTraziStavku.TabIndex = 4;
            this.btnTraziStavku.Text = "Pronadi Stavku";
            this.btnTraziStavku.UseVisualStyleBackColor = true;
            this.btnTraziStavku.Click += new System.EventHandler(this.btnTraziStavku_Click);
            // 
            // lblCijena
            // 
            this.lblCijena.AutoSize = true;
            this.lblCijena.Font = new System.Drawing.Font("Consolas", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblCijena.Location = new System.Drawing.Point(964, 28);
            this.lblCijena.Name = "lblCijena";
            this.lblCijena.Size = new System.Drawing.Size(132, 41);
            this.lblCijena.TabIndex = 1;
            this.lblCijena.Text = "Cijena";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label5.Location = new System.Drawing.Point(4, 283);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(212, 31);
            this.label5.TabIndex = 6;
            this.label5.Text = "Cijena Stavke :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(4, 203);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(211, 31);
            this.label4.TabIndex = 6;
            this.label4.Text = "Naziv Stavke : ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(4, 115);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(167, 31);
            this.label3.TabIndex = 6;
            this.label3.Text = "ID Stavke : ";
            // 
            // txtboxCijenaStavke
            // 
            this.txtboxCijenaStavke.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.txtboxCijenaStavke.Location = new System.Drawing.Point(10, 317);
            this.txtboxCijenaStavke.Name = "txtboxCijenaStavke";
            this.txtboxCijenaStavke.Size = new System.Drawing.Size(537, 29);
            this.txtboxCijenaStavke.TabIndex = 5;
            this.txtboxCijenaStavke.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtBoxNazivStavke
            // 
            this.txtBoxNazivStavke.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.txtBoxNazivStavke.Location = new System.Drawing.Point(10, 237);
            this.txtBoxNazivStavke.Name = "txtBoxNazivStavke";
            this.txtBoxNazivStavke.Size = new System.Drawing.Size(537, 29);
            this.txtBoxNazivStavke.TabIndex = 5;
            this.txtBoxNazivStavke.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtboxIDstavke
            // 
            this.txtboxIDstavke.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.txtboxIDstavke.Location = new System.Drawing.Point(10, 149);
            this.txtboxIDstavke.Name = "txtboxIDstavke";
            this.txtboxIDstavke.Size = new System.Drawing.Size(537, 29);
            this.txtboxIDstavke.TabIndex = 5;
            this.txtboxIDstavke.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // suceljeBlagajna
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1148, 559);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "suceljeBlagajna";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "FIS 2021";
            this.Load += new System.EventHandler(this.suceljeBlagajna_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
           
        }

        #endregion

        private System.Windows.Forms.TextBox txtStavka;
        private System.Windows.Forms.TextBox txtUplaceno;
        private System.Windows.Forms.Label lblObracun;
        private System.Windows.Forms.Button btnIzracunaj;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblCijena;
        private System.Windows.Forms.Button btnTraziStavku;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtboxCijenaStavke;
        private System.Windows.Forms.TextBox txtBoxNazivStavke;
        private System.Windows.Forms.TextBox txtboxIDstavke;
        private System.Windows.Forms.Button Naplata;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RichTextBox rchTextbox;
        private System.Windows.Forms.Label lblTrenutniKorisnik;
    }
}