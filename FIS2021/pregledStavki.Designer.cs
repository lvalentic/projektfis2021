﻿namespace FIS2021
{
    partial class pregledStavki
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(pregledStavki));
            this.btnFind = new System.Windows.Forms.Button();
            this.rchtextbox = new System.Windows.Forms.RichTextBox();
            this.txtboxFind = new System.Windows.Forms.TextBox();
            this.btnJson = new System.Windows.Forms.Button();
            this.btnXml = new System.Windows.Forms.Button();
            this.btnSve = new System.Windows.Forms.Button();
            this.btnToXml = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnFind
            // 
            this.btnFind.Location = new System.Drawing.Point(12, 47);
            this.btnFind.Name = "btnFind";
            this.btnFind.Size = new System.Drawing.Size(153, 29);
            this.btnFind.TabIndex = 0;
            this.btnFind.Text = "Traži";
            this.btnFind.UseVisualStyleBackColor = true;
            this.btnFind.Click += new System.EventHandler(this.btnFind_Click);
            // 
            // rchtextbox
            // 
            this.rchtextbox.Location = new System.Drawing.Point(12, 133);
            this.rchtextbox.Name = "rchtextbox";
            this.rchtextbox.Size = new System.Drawing.Size(1026, 401);
            this.rchtextbox.TabIndex = 1;
            this.rchtextbox.Text = "";
            // 
            // txtboxFind
            // 
            this.txtboxFind.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.txtboxFind.Location = new System.Drawing.Point(12, 12);
            this.txtboxFind.Name = "txtboxFind";
            this.txtboxFind.Size = new System.Drawing.Size(676, 29);
            this.txtboxFind.TabIndex = 2;
            this.txtboxFind.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btnJson
            // 
            this.btnJson.Location = new System.Drawing.Point(12, 98);
            this.btnJson.Name = "btnJson";
            this.btnJson.Size = new System.Drawing.Size(153, 29);
            this.btnJson.TabIndex = 3;
            this.btnJson.Text = "JSON";
            this.btnJson.UseVisualStyleBackColor = true;
            this.btnJson.Click += new System.EventHandler(this.btnJson_Click);
            // 
            // btnXml
            // 
            this.btnXml.Location = new System.Drawing.Point(330, 47);
            this.btnXml.Name = "btnXml";
            this.btnXml.Size = new System.Drawing.Size(153, 29);
            this.btnXml.TabIndex = 4;
            this.btnXml.Text = "Load XML";
            this.btnXml.UseVisualStyleBackColor = true;
            this.btnXml.Click += new System.EventHandler(this.btnXml_Click);
            // 
            // btnSve
            // 
            this.btnSve.Location = new System.Drawing.Point(171, 47);
            this.btnSve.Name = "btnSve";
            this.btnSve.Size = new System.Drawing.Size(153, 29);
            this.btnSve.TabIndex = 5;
            this.btnSve.Text = "Sve";
            this.btnSve.UseVisualStyleBackColor = true;
            this.btnSve.Click += new System.EventHandler(this.btnSve_Click);
            // 
            // btnToXml
            // 
            this.btnToXml.Location = new System.Drawing.Point(171, 98);
            this.btnToXml.Name = "btnToXml";
            this.btnToXml.Size = new System.Drawing.Size(153, 29);
            this.btnToXml.TabIndex = 6;
            this.btnToXml.Text = "Export XML";
            this.btnToXml.UseVisualStyleBackColor = true;
            this.btnToXml.Click += new System.EventHandler(this.btnToXml_Click);
            // 
            // pregledStavki
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1050, 546);
            this.Controls.Add(this.btnToXml);
            this.Controls.Add(this.btnSve);
            this.Controls.Add(this.btnXml);
            this.Controls.Add(this.btnJson);
            this.Controls.Add(this.txtboxFind);
            this.Controls.Add(this.rchtextbox);
            this.Controls.Add(this.btnFind);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "pregledStavki";
            this.Text = "FIS2021";
            this.Load += new System.EventHandler(this.pregledStavki_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnFind;
        private System.Windows.Forms.RichTextBox rchtextbox;
        private System.Windows.Forms.TextBox txtboxFind;
        private System.Windows.Forms.Button btnJson;
        private System.Windows.Forms.Button btnXml;
        private System.Windows.Forms.Button btnSve;
        private System.Windows.Forms.Button btnToXml;
    }
}